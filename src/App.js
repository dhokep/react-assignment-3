/* eslint-disable react/react-in-jsx-scope */
import React, { useState } from 'react';
import './App.css';
import { InputText } from './Components/InputText';
import { InputFile } from './Components/InputFile';
import { Range } from './Components/Range';
import { Options } from './Components/Options';
import { GetDate } from './Components/GetDate';
import { Radio } from './Components/Radio';


function App() {

  const [iptext, setIptext] = useState('');
  const [op, setOp] = useState();
  const [ipfile, setIpfile] = useState('');
  const [range, setRange] = useState(0);
  const [getDate, setDate] = useState('2021-06-30');
  // eslint-disable-next-line no-unused-vars
  const [getRadio, setRadio] = useState('');

  const handleOption = (options) => {
    console.log(options);
    setOp(options);
  };

  const handleIptext = (ip) => {
    console.log(ip);
    setIptext(ip);
  };

  const handleIpfile = (fileData) => {
    console.log(fileData);
    setIpfile(fileData);
  };

  const handleRange = (r) => {
    console.log(r);
    setRange(r);
  };

  const handleDate = (d) => {
    console.log(d);
    setDate(d);
  };

  const handleRadio = (rad) => {
    console.log(rad);
    setRadio(rad); 
  };

  return (   

    <div className="App">

      <InputText type='text' value={iptext} placeholder='Enter Something' onChange={handleIptext}/>
      <InputFile value={ipfile} onChange={handleIpfile}/>
      <Range value={range} min='0' max='100' onChange={handleRange} label='Range'/>
      <GetDate value={getDate} onChange={handleDate}/>
      <Radio label='Male' value='male' name='gender' onChange={handleRadio}/>
      <Radio label='Female' value='female' name='gender' onChange={handleRadio}/>
      <Options 
        data={[
          {value: 1, label: 'one'},
          {value: 2, label: 'two'},
          {value: 3, label: 'three'},
          {value: 4, label: 'four'}
        ]}
        value = {op}
        onChange={handleOption}
      />
    </div>
  );
}

export default App;
