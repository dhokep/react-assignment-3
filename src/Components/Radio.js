/* eslint-disable react/prop-types */
import React from 'react'

export const Radio = ({label, name, value, onChange}) => {

  const handleChange = (e) => {
    const {value} = e.target;
    onChange(value);
  };

  return (
    <div>
      <label>{label}</label>
      <input type="radio" name={name} value={value} onChange={handleChange}/>      
    </div>
  )
}
