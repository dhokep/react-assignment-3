/* eslint-disable react/prop-types */
import React from 'react'

export const Range = ({label, min, max, onChange}) => {

  const handleChange = (e) => {
    const {value} = e.target;
    onChange(value);
  }

  return (
    <div>
      <label>{label}</label>
      <input type="range" min={min} max={max} onChange={handleChange}/>
    </div>
  )
}
