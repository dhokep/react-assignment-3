/* eslint-disable react/prop-types */
import React from 'react'

export const InputText = ({value, type, placeholder, onChange}) => {

  const handleChange = (e) => {
    const {value} = e.target;
    onChange(value);
  }

  return (
    <div>
      <input type={type} value={value} placeholder={placeholder} onChange={handleChange}/> 
    </div>
  )
}
