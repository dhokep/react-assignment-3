/* eslint-disable react/prop-types */
import React from 'react'

export const InputFile = ({value, onChange}) => {

  const handleChange = (event) => {
    const {value} = event.target;
    onChange(value);
  } 

  return (
    <div>
      <input type="file" value={value} onChange={handleChange}/>
    </div>
  )
}
