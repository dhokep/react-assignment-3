/* eslint-disable react/prop-types */
import React from 'react'

export const GetDate = ({value, onChange}) => {

  const handleChange =(e) => {
    const {value} = e.target;
    onChange(value);
  }

  return (
    <div>
      <input type="date" value={value} onChange={handleChange}/>
    </div>
  )
}
