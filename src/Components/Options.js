/* eslint-disable react/prop-types */
import React from 'react'
import PropTypes from 'prop-types';

export const Options = ({ data, value, onChange}) => {

  const handleChange = (e) => {
    const {value} = e.target;
    onChange(value);
  };

  return (
    <div>
      <select value={value} onChange={handleChange}>
        <option>Please Select the option</option>
        {data.map((item, key) => (
          <option key={key} value={item.value}>{item.label}</option>))}
      </select>
    </div>
  )
}

Options.PropTypes = {
  value: PropTypes.string,
  data: PropTypes.array
}

Options.defaultProps = {
  value: '',
  data: ['']  
}