# Assignment 3 - Components

Added different types of components in the project :

- InputText
- InputFile
- Date
- Options
- Radio
- Range

#### Run the application

```http
  npm start
```

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

![Logo](https://cdn.newswire.com/files/x/68/fe/e25c6430156095d6f4982d0ec1ab.jpg)
